#!/bin/bash
set -euo pipefail

# bash
npm install -g @cspell/dict-bash
cspell link add @cspell/dict-bash
# companies
npm install -g @cspell/dict-companies
cspell link add @cspell/dict-companies
# css
npm install -g @cspell/dict-css
cspell link add @cspell/dict-css
# docker
npm install -g @cspell/dict-docker
cspell link add @cspell/dict-docker
# american english
npm install -g @cspell/dict-en_us
cspell link add @cspell/dict-en_us
# british english
npm install -g @cspell/dict-en-gb
cspell link add @cspell/dict-en-gb
# filetypes
npm install -g @cspell/dict-filetypes
cspell link add @cspell/dict-filetypes
# golang
npm install -g @cspell/dict-golang
cspell link add @cspell/dict-golang
# licenses
npm install -g @cspell/dict-public-licenses
cspell link add @cspell/dict-public-licenses
# markdown
npm install -g @cspell/dict-markdown
cspell link add @cspell/dict-markdown
# nodejs
npm install -g @cspell/dict-node
cspell link add @cspell/dict-node
# npm
npm install -g @cspell/dict-npm
cspell link add @cspell/dict-npm
# python
npm install -g @cspell/dict-python
cspell link add @cspell/dict-python
# ruby
npm install -g @cspell/dict-ruby
cspell link add @cspell/dict-ruby
# shell
npm install -g @cspell/dict-shell
cspell link add @cspell/dict-shell
# software jargon
npm install -g @cspell/dict-software-terms
cspell link add @cspell/dict-software-terms
# SQL
npm install -g @cspell/dict-sql
cspell link add @cspell/dict-sql
# vue
npm install -g @cspell/dict-vue
cspell link add @cspell/dict-vue